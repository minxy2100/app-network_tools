#!/bin/sh

#exit on any failure in the pipeline
set -e

# --------------------------------------------------
# pre-commit
# --------------------------------------------------
# Contains the standard set of tasks to run before
# committing changes to the repo. If any tasks fail
# then the commit will be aborted.
# --------------------------------------------------

printf "%b" "Running pre-commit hooks...\\n"

# security audit on the code
npm audit --registry=https://registry.npmjs.org

# lint the code
npm run lint

# generate pronghorn.json from jsdocs & add any changes
npm run generate
npm run docs
git add pronghorn.json
git add api.md

# test the code
npm run test:unit

printf "%b" "Finished running pre-commit hooks\\n"
