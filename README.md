# app-network_tools

An application to be used in Itential Automation Platform providing basic network operations, including a wrapper for dig tools using the [NodeJS DNS module](https://nodejs.org/api/dns.html).

Allows DNS/dig commands to be used with Workflow Builder.

  - [How to Install](#how-to-install)
  - [How to Run](#how-to-run)
  - [Available tasks](#available-tasks)
    - [resolveHostName](#resolvehostname)
    - [checkRecordExists](#checkrecordexists)
    - [reverseIpAddr](#reverseipaddr)
    - [checkPTRRecordExists](#checkptrrecordexists)
    - [checkPortConnection](#checkPortConnection)
  - [Sample Workflows](#sample-workflows)
  - [Contributing to app-network_tools](#contributing-to-app-network_tools)
  - [License & Maintainers](#license--maintainers)
    - [Product License](#product-license)


## How to Install

In order to install app-network_tools, please use the App-Artifacts tool (Available for Itential customers via Nexus repo)

## How to Run

After installing (via App-Artifacts) click the play button on the artifact card, or navigate to Workflow Builder and press the play button next to the **app-network_tools Test** workflow.

## Available tasks

### resolveHostName

*  `resolveHostName` performs DNS Lookup on one or 'ANY' DNS Record type and returns an array of record objects.
The function wraps [dns.resolve](https://nodejs.org/api/dns.html#dns_dns_resolve_hostname_rrtype_callback). 
Provide a hostname (e.g. google.com) and a record type (e.g. A), the task will return an array of objects or throw a DNS error. The getLastRecord option will pop the last item in the array and return an object.

![resolveHostName](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/resolveHostName.png)

### checkRecordExists

*  `checkRecordExists` performs DNS Lookup on one DNS Record type and returns a boolean true if the record exists. Use this task with an evaluation (Eval) task. 
The function wraps [dns.resolve](https://nodejs.org/api/dns.html#dns_dns_resolve_hostname_rrtype_callback). 
Provide a hostname (e.g. google.com) and a record type (e.g. A), the task will return a boolean.

![checkRecordExists](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/checkRecordExists.png)

### reverseIpAddr

*  `reverseIpAddr` performs Reverse DNS Lookup on only PTR records and returns an array of record objects. 
The function wraps [dns.reverse](https://nodejs.org/api/dns.html#dns_dns_reverse_ip_callback).
Provide an IP Address (e.g. 8.8.8.8), the task will return an array of objects or throw a DNS error. The getLastRecord option will pop the last item in the array and return an object.

![reverseIpAddr](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/reverseIpAddr.png)

### checkPTRRecordExists

*  `checkPTRRecordExists` performs Reverse DNS Lookup on only PTR records and returns a boolean true if the record exists. Use this task with an evaluation (Eval) task. 
The function wraps [dns.reverse](https://nodejs.org/api/dns.html#dns_dns_reverse_ip_callback). 
Provide an IP Address (e.g. 8.8.8.8), the task will return a boolean.

![checkPTRRecordExists](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/checkPTRRecordExists.png)

### checkPortConnection

* `checkPortConnection` checks the connection to a hostname on a port, and performs retries if necessary. Returns true on connection success, and returns false on timeout or error.

## Sample Workflow

`IAP Artifacts Network Tools - Test` allows users to test the functionality of all available tasks of app-network_tools using customized values for the `hostName`, `ipAddress`, `recordType`, `port`, `retries`, and `timeout` variables.

![app-network_tools Test workflow](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/sampleworkflow.png)

Click the Play button (&#9658;) on the Workflow Builder page to start the workflow.

![app-network_tools Test start workflow](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/startworkflow.png)

Click "Start" after entering Job Description information.

![app-network_tools Test job description](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/jobdescription.png)

Replace "null" with desired values, with strings in quotes, and click "Start."

![app-network_tools Test variables](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/workflowvariables.png)

Once the automated tasks are complete, click "Work Task" on the Details page for the workflow to view the report showing the inputs and outputs for each of the available tasks.

![app-network_tools Test workflow report](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/images/report.png)

## Contributing to app-network_tools

Please check out the [Contributing Guidelines](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/CONTRIBUTING.md).

## License & Maintainers

### Product License

[Apache 2.0](https://gitlab.com/itentialopensource/applications/app-network_tools/raw/master/LICENSE)
