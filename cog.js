/* eslint-disable class-methods-use-this */
/* @copyright Itential, LLC 2019 */

/* eslint import/no-dynamic-require: warn */
/* eslint object-curly-newline: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint camelcase: warn  */

// Set globals
/* global log */

/* Required libraries.  */
const dns = require('dns');
const net = require('net');
// const xmldom = require('xmldom');

/**
 * This is the app/interface into Dig
 *
 * @name pronghornTitleBlock
 * @pronghornId @itentialopensource/app-network_tools
 * @title network_tools
 * @export NetworkTools
 * @type Application
 * @summary Basic network operations, including a Node.js DNS wrapper
 * @displayName Network Tools
 * @src cog.js
 * @encrypted false
 * @roles admin
 */

class Dns {
  /**
   * Retrieves records associated with a given host name
   *
   * @description Ask a dns/dig question to find records using host name.
   * @pronghornType method
   * @name resolveHostName
   * @summary Find records using host name
   * @param {string} hostName - e.g. google.com
   * @variableTooltip {hostName} e.g. google.com
   * @param {string} recordType - e.g. A or /A/
   * @variableTooltip {recordType} e.g. A or /A/
   * @param {boolean} getLastRecord - return only the last record object found by dig.
   * @variableTooltip {getLastRecord} Get last object returned only?
   * @param callback
   * @return {object} response Response from dig
   * @task true
   * @roles admin
   * @example
   * {
   *    "hostName": "google.com",
   *    "recordType": "A",
   *    "getLastRecord": true
   * }
   */
  resolveHostName(hostName, recordType = 'ANY', getLastRecord = false, callback) {
    if (typeof hostName !== 'string' || hostName === '') {
      return callback(null, 'You must specify hostName as string.');
    }
    try {
      log.debug(`In resolveHostName, with ${hostName}, ${recordType}, ${getLastRecord}`);
      return dns.resolve(hostName, recordType, (error, addresses) => {
        if (error) {
          log.error(error);
          return callback(null, error);
        }
        let answer = addresses;
        // If they pass the getLastRecord flag, pop the last answer from the array.
        if (answer && getLastRecord) {
          Object.keys(answer).forEach((key) => {
            answer = answer[key] ? answer[key] : '';
          });
        }
        return callback(answer, null);
      });
    } catch (err) {
      return callback(null, err);
    }
  }

  /**
   * Returns a boolean true if there's a record associated with the host name
   *
   * @description Does record lookup and returns a boolean
   * @pronghornType method
   * @name checkRecordExists
   * @summary Check if a record with the same name exists
   * @param {string} hostName - e.g. google.com
   * @variableTooltip {hostName} e.g. google.com
   * @param {string} recordType - e.g. A or /A/
   * @variableTooltip {recordType} e.g. A or /A/
   * @param callback
   * @return {object} response Response from dig
   * @route {GET} /checkRecordExists/:hostName/:recordType
   * @roles admin
   * @task true
   * @example
   * {
   *    "hostName": "google.com",
   *    "recordType": "A"
   * }
   */
  checkRecordExists(hostName, recordType, callback) {
    if (typeof hostName !== 'string' || hostName === '') {
      return callback(null, 'You must specify hostName as string.');
    }
    if (recordType === '' || !recordType || typeof recordType !== 'string') {
      return callback(null, 'You must specify record type as string.');
    }
    if (recordType === 'ANY') {
      return callback(null, 'ANY record type check is not yet supported.');
    }
    // Todo: Add checks for supported Record Types
    try {
      return this.resolveHostName(hostName, recordType, false, (data, err) => {
        // if the error is a DNS ENODATA error, then PTR record does not exist. Return false.
        if (err && (err.code === 'ENODATA' || err.code === 'ENOTFOUND')) {
          return callback(false, null);
        }
        // if it's some other error, we need to notify the user
        if (err) {
          return callback(null, err);
        }
        // Check the response length
        const response = data && Object.keys(data) ? Object.keys(data).length > 0 : false;
        return callback(response, null);
      });
    } catch (err) {
      return callback(null, err);
    }
  }

  /**
   * Does DNS reverse lookup on the specified IP address. Returns the associated PTR host name.
   *
   * @description Ask a dns/dig question to find PTR record using IP address.
   * @pronghornType method
   * @name reverseIpAddr
   * @summary Find PTR record using IP address
   * @param {string} ipAddress - e.g. 8.8.8.4
   * @variableTooltip {ipAddress} e.g. 8.8.8.4
   * @param {boolean} getLastRecord - return only the last record object found by dig.
   * @variableTooltip {getLastRecord} Get last object returned only?
   * @param callback
   * @return {object} response Response from dig
   * @roles admin
   * @task true
   * @example
   * {
   *    "ipAddress": "8.8.8.4",
   *    "getLastRecord": true
   * }
   */
  reverseIpAddr(ipAddress, getLastRecord = false, callback) {
    if (typeof ipAddress !== 'string' || ipAddress === '') {
      return callback(null, 'You must specify ipAddress as string.');
    }
    try {
      return dns.reverse(ipAddress, (error, hostName) => {
        if (error) {
          return callback(null, error);
        }
        let answer = hostName;
        // If they pass the getLastRecord flag, pop the last answer from the array.
        if (answer && getLastRecord) {
          answer = answer.pop();
        }
        return callback(answer, null);
      });
    } catch (err) {
      return callback(null, err);
    }
  }

  /**
   * Checks for the existence of a PTR record by doing a DNS reverse lookup.
   * Returns a boolean true if record exists.
   *
   * @description Does reverse lookup and returns a boolean
   * @pronghornType method
   * @name checkPTRRecordExists
   * @summary Check if the PTR record exists
   * @param {string} ipAddress - e.g. 8.8.8.4
   * @variableTooltip {ipAddress} e.g. 8.8.8.4
   * @param callback
   * @return {object} response Response from dig
   * @route {GET} /checkPTRRecordExists/:ipAddress
   * @roles admin
   * @task true
   * @example
   * {
   *    "ipAddress": "8.8.8.4"
   * }
   */
  checkPTRRecordExists(ipAddress, callback) {
    if (typeof ipAddress !== 'string' || ipAddress === '') {
      return callback(null, 'You must specify ipAddress as string.');
    }
    try {
      return this.reverseIpAddr(ipAddress, false, (data, err) => {
        // if the error is a DNS ENOTFOUND error, then PTR record does not exist. Return false.
        if (err && (err.code === 'ENODATA' || err.code === 'ENOTFOUND')) {
          return callback(false, null);
        }
        // if it's some other error, we need to notify the user
        if (err) {
          log.error(JSON.stringify(err));
          return callback(null, err);
        }
        // Check the response length
        const response = data ? data.length > 0 : false;
        return callback(response, null);
      });
    } catch (err) {
      return callback(null, err);
    }
  }

  /**
   * Checks the connection to a host name on a port, and performs retries if necessary.
   * Returns a boolean true if connection exists.
   *
   * @description Does port connection check and returns a boolean
   * @pronghornType method
   * @name checkPortConnection
   * @summary Check if connection to host on port exists, with retries
   * @param {string} hostName - e.g. google.com
   * @variableTooltip {hostName} e.g. google.com
   * @param {number} port - e.g. 80
   * @variableTooltip {port} e.g. 80
   * @param {number} [retries=5]
   * @variableTooltip {retries} e.g. 5
   * @param {number} [timeout=1000]
   * @variableTooltip {timeout} e.g. 5000
   * @param callback
   * @return {object} portConnection Result port connection test
   * @route {POST} /checkPortConnection
   * @roles admin
   * @task true
   * @example
   * {
    *    "hostName": "google.com",
    *    "port": 80,
    *    "retries": 5,
    *    "timeout": 5000
    * }
   */
  // eslint-disable-next-line consistent-return
  checkPortConnection(hostName, port, retries, timeout, callback) {
    if (hostName === '' || typeof hostName !== 'string') {
      return callback(null, 'You must specify hostName as a string.');
    }
    if (!port || typeof port !== 'number') {
      return callback(null, 'You must specify port as a number.');
    }
    if (!retries || typeof retries !== 'number') {
      return callback(null, 'You must specify retries as a number.');
    }
    if (!timeout || typeof timeout !== 'number' || timeout < 0 || timeout > 10000) {
      return callback(null, 'You must specify timeout as a number greater than 0 and less than 10000.');
    }
    // let attempt = 0;
    const tryConnect = (attempt) => {
      const client = new net.Socket();
      client.on('connect', () => {
        client.destroy();
        return callback(true, null);
      });
      // eslint-disable-next-line consistent-return
      client.on('timeout', () => {
        // attempt += 1;
        client.destroy();
        if (attempt <= retries) {
          tryConnect(1 + attempt);
        } else {
          return callback(false, null);
        }
      });
      // client.on('close', () => {
      // });

      // eslint-disable-next-line consistent-return
      client.on('error', () => {
        client.destroy();
        if (attempt <= retries) {
          tryConnect(1 + attempt);
        } else {
          return callback(false, null);
        }
      });
      client.setTimeout(timeout);
      client.connect(port, hostName);
    };
    tryConnect(0);
  }
}

module.exports = new Dns();
