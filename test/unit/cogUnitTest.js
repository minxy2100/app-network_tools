// Set globals
/* global describe it xit log */
/* eslint global-require:warn */

// include required items for testing & logging
/* eslint-disable import/no-extraneous-dependencies */
const assert = require('assert');
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const td = require('testdouble');
const { EventEmitter } = require('events');

const anything = td.matchers.anything();
const dns = td.replace('dns');
const net = td.replace('net');

let logLevel = 'none';
// const attemptTimeout = 25000;
const isRapidFail = false;

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = new (winston.Logger)({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});


// require the app that we are going to be using
const cog = require('../../cog.js');


// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] dns App Test', () => {
  describe('dns Class Tests', () => {
    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        fs.exists('package.json', (val) => {
          assert.equal(true, val);
          done();
        });
      });
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        fs.exists('README.md', (val) => {
          assert.equal(true, val);
          log.log('README.md exists');
          done();
        });
      });
    });

    describe('#checkRecordExists', () => {
      it('should return an error when hostname is empty.', (done) => {
        cog.checkRecordExists('', 'A', (res, err) => {
          assert.equal(res, null);
          assert.equal(err, 'You must specify hostName as string.');
          done();
        });
      });
      it('should return an error when hostname is not string.', (done) => {
        cog.checkRecordExists({}, 'A', (res, err) => {
          assert.equal(res, null);
          assert.equal(err, 'You must specify hostName as string.');
          done();
        });
      });
      it('should return an error when recordType is empty string.', (done) => {
        cog.checkRecordExists('google.com', '', (res, err) => {
          assert.equal(res, null);
          assert.equal(err, 'You must specify record type as string.');
          done();
        });
      });
      it('should return an error when recordType is null.', (done) => {
        cog.checkRecordExists('google.com', null, (res, err) => {
          assert.equal(res, null);
          assert.equal(err, 'You must specify record type as string.');
          done();
        });
      });
      it('should return an error when recordType is not string.', (done) => {
        cog.checkRecordExists('google.com', {}, (res, err) => {
          assert.equal(res, null);
          assert.equal(err, 'You must specify record type as string.');
          done();
        });
      });
      it('should return an error when recordType is ANY.', (done) => {
        cog.checkRecordExists('google.com', 'ANY', (res, err) => {
          assert.equal(res, null);
          assert.equal(err, 'ANY record type check is not yet supported.');
          done();
        });
      });
      it('should return false when DNS ENODATA error.', (done) => {
        td.when(dns.resolve(anything, anything)).thenCallback({ code: 'ENODATA' }, null);
        cog.checkRecordExists('google.com', 'A', (res, err) => {
          assert.equal(res, false);
          assert.equal(err, null);
          done();
        });
      });
      it('should return error message when dns error.', (done) => {
        const errMsg = 'my error message';
        td.when(dns.resolve(anything, anything)).thenCallback(errMsg, null);
        cog.checkRecordExists('google.com', 'A', (res, err) => {
          assert.equal(res, null);
          assert.equal(err, errMsg);
          done();
        });
      });
      it('should return true when records found.', (done) => {
        td.when(dns.resolve(anything, anything)).thenCallback(null, { 1: '1', 2: '2' });
        cog.checkRecordExists('google.com', 'A', (res, err) => {
          assert.equal(res, true);
          assert.equal(err, null);
          done();
        });
      });
      it('should return false when no records found.', (done) => {
        td.when(dns.resolve(anything, anything)).thenCallback(null, {});
        cog.checkRecordExists('google.com', 'A', (res, err) => {
          assert.equal(res, false);
          assert.equal(err, null);
          done();
        });
      });
      /*
            UNABLE TO TEST THE LINE     if (answer && getLastRecord) {
            IN COG SINCE getLastRecord IS ALWAYS FALSE

            WHY recordType IS ALWAYS ANY IN resolveHostName function definition
      */
    });
    describe('#checkPTRRecordExists', () => {
      mocha.afterEach(() => {
        td.reset();
      });
      it('should return error when ipAddress is not type string.', (done) => {
        cog.checkPTRRecordExists({}, (data, err) => {
          assert.equal(data, null);
          assert.equal(err, 'You must specify ipAddress as string.');
          done();
        });
      });
      it('should return false when dns returns ENOTFOUND error.', (done) => {
        td.when(dns.reverse(anything)).thenCallback(null, { code: 'ENOTFOUND' });
        cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
          assert.equal(data, false);
          assert.equal(err, null);
          done();
        });
      });
      it('should return error message on every other error.', (done) => {
        const errMsg = { message: 'my error code' };
        td.when(dns.reverse(anything)).thenCallback(errMsg, null);
        cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
          assert.equal(data, null);
          assert.equal(err, errMsg);
          done();
        });
      });
      it('should return false if no record found.', (done) => {
        td.when(dns.reverse(anything)).thenCallback(null, []);
        cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
          assert.equal(data, false);
          assert.equal(err, null);
          done();
        });
      });
      it('should return true if any record found.', (done) => {
        td.when(dns.reverse(anything)).thenCallback(null, [1, 2, 3]);
        cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
          assert.equal(data, true);
          assert.equal(err, null);
          done();
        });
      });
      // it('should return error when reverseIpAddr throws an error.', (done) => {
      //   const throwMsg = { message: 'Throwing error!' };
      //   td.when(dns.reverse(anything)).thenThrow(throwMsg);
      //   cog.checkPTRRecordExists('1.2.3.4', (data, err) => {
      //     assert.equal(data, null);
      //     assert.equal(err, throwMsg);
      //     done();
      //   });
      // });
    });
    describe('#checkPortConnection', () => {
      describe('verify inputs', () => {
        it('should exit with an error when hostname is an empty string.', (done) => {
          cog.checkPortConnection('', 80, 5, 5000, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify hostName as a string.');
            done();
          });
        });
        it('should exit with an error when hostname is not a string.', (done) => {
          cog.checkPortConnection({}, 80, 5, 5000, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify hostName as a string.');
            done();
          });
        });
        it('should exit with an error when port is null.', (done) => {
          cog.checkPortConnection('8.8.8.8', null, 5, 5000, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify port as a number.');
            done();
          });
        });
        it('should exit with an error when port is not a number.', (done) => {
          cog.checkPortConnection('8.8.8.8', '80', 5, 5000, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify port as a number.');
            done();
          });
        });
        it('should exit with an error when retries is null.', (done) => {
          cog.checkPortConnection('8.8.8.8', 80, null, 5000, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify retries as a number.');
            done();
          });
        });
        it('should exit with an error when retries is not a number.', (done) => {
          cog.checkPortConnection('8.8.8.8', 80, '5', 5000, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify retries as a number.');
            done();
          });
        });
        xit('TODO: should not exit with an error when retries is 0');
        it('should exit with an error when timeout is null.', (done) => {
          cog.checkPortConnection('8.8.8.8', 80, 5, null, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify timeout as a number greater than 0 and less than 10000.');
            done();
          });
        });
        it('should exit with an error when timeout is not a number.', (done) => {
          cog.checkPortConnection('8.8.8.8', 80, 5, '5000', (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify timeout as a number greater than 0 and less than 10000.');
            done();
          });
        });
        it('should exit with an error when timeout is not a positive number.', (done) => {
          cog.checkPortConnection('8.8.8.8', 80, 5, -1000, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify timeout as a number greater than 0 and less than 10000.');
            done();
          });
        });
        it('should exit with an error when timeout is greater than 10s.', (done) => {
          cog.checkPortConnection('8.8.8.8', 80, 5, 20000, (data, err) => {
            assert.equal(data, null);
            assert.equal(err, 'You must specify timeout as a number greater than 0 and less than 10000.');
            done();
          });
        });
      });
      describe('test tryConnect', () => {
        let client;

        // create an event listener and attach the methods to stub in order to fire mock events
        const createClient = () => {
          client = new EventEmitter();
          client.connect = td.func();
          client.setTimeout = td.func();
          client.destroy = td.func();
          return client;
        };

        mocha.beforeEach(() => {
          // create a new "client" each time it is asked for by tryConnect function
          td.when(new net.Socket()).thenReturn(createClient());
          // destroy all attached event listeners to mock the Socket.prototype.destroy method
          td.when(client.destroy()).thenDo(() => client.removeAllListeners());
        });
        mocha.afterEach(() => {
          td.reset();
        });

        it('should return true on successful connection.', (done) => {
          td.when(client.connect(anything, anything)).thenDo(() => client.emit('connect'));
          cog.checkPortConnection('hostname', 80, 5, 5000, (data, err) => {
            assert.equal(data, true);
            assert.equal(err, null);
            done();
          });
        });
        it('should return false on connection timeout.', (done) => {
          td.when(client.setTimeout(anything)).thenDo(() => client.emit('timeout'));
          cog.checkPortConnection('hostname', 80, 5, 5000, (data, err) => {
            assert.equal(data, false);
            assert.equal(err, null);
            done();
          });
        });
        xit('should return false on connection timeout with 0 retries');
        it('should return false on connection error.', (done) => {
          td.when(client.connect(anything, anything)).thenDo(() => client.emit('error'));
          cog.checkPortConnection('hostname', 80, 5, 5000, (data, err) => {
            assert.equal(data, false);
            assert.equal(err, null);
            done();
          });
        });
        xit('TODO: should return false on connection error with 0 retries');
      });
    });
  });
});
