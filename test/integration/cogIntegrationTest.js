/* eslint-disable no-unused-vars */
// Set globals
/* global describe it log pronghornProps */

// include required items for testing & logging
const assert = require('assert');
const mocha = require('mocha');
const winston = require('winston');
const itentialPromisify = require('@itentialopensource/itentialpromisify');
// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const attemptTimeout = 25000;

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = new (winston.Logger)({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

// require the app that we are going to be using
const Dig = require('../../cog.js');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Dig Adapter Test', () => {
  describe('Dig Class Tests', () => {
    const a = Dig;

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    const mochaAsync = fn => (done) => {
      fn.call().then(done, (err) => {
        done(err);
      });
    };

    const testParams = {};
    testParams.recordType = 'A';
    testParams.hostname = 'google.com';
    describe('#resolveHostName', () => {
      it('should get record and record should be A only', mochaAsync(async () => {
        a.resolveHostName = itentialPromisify(a.resolveHostName);
        const result = await a.resolveHostName(testParams.hostname, testParams.recordType, true);
        assert.notEqual(result, undefined);
        assert.notEqual(result, []);
      })).timeout(attemptTimeout);
    });
    describe('#checkRecordExists', () => {
      it('should return record exists unless Google goes down. Oh oh.', mochaAsync(async () => {
        a.checkRecordExists = itentialPromisify(a.checkRecordExists);
        const result = await a.checkRecordExists(testParams.hostname, testParams.recordType);
        assert.equal(result, true);
      })).timeout(attemptTimeout);
    });

    testParams.ipAddress = '8.8.8.8';
    testParams.reverseRecord = 'dns.google';
    describe('#reverseIpAddr', () => {
      it('should return record exists unless Google goes down. Oh oh.', mochaAsync(async () => {
        a.reverseIpAddr = itentialPromisify(a.reverseIpAddr);
        const result = await a.reverseIpAddr(testParams.ipAddress, true);
        assert.equal(result, testParams.reverseRecord);
      })).timeout(attemptTimeout);
    });

    describe('#checkPTRRecordExists', () => {
      it('should return record exists unless Google goes down. Oh oh.', mochaAsync(async () => {
        a.checkPTRRecordExists = itentialPromisify(a.checkPTRRecordExists);
        const result = await a.checkPTRRecordExists(testParams.ipAddress);
        assert.equal(result, true);
      })).timeout(attemptTimeout);
    });
  });
});
