
## 0.0.8 [11-11-2019]

* Update name of sample workflow in README.md

See merge request itentialopensource/applications/app-network_tools!12

---

## 0.0.7 [11-11-2019]

* Update name of sample workflow in README.md

See merge request itentialopensource/applications/app-network_tools!12

---

## 0.0.6 [11-07-2019]

* Change keyword from App to Application

See merge request itentialopensource/applications/app-network_tools!10

---

## 0.0.5 [11-01-2019]

* change keyword from Application to App; ignore utils/phJson

See merge request itentialopensource/applications/app-network_tools!8

---

## 0.0.4 [11-01-2019]

* add expected keywords to package.json for App-Artifacts

See merge request itentialopensource/applications/app-network_tools!7

---

## 0.0.3 [11-01-2019]

* update image links to point to Gitlab public project in order to fix broken npmjs broken links

See merge request itentialopensource/applications/app-network_tools!6

---

## 0.0.2 [11-01-2019]

* Patch/dsup 765

See merge request itentialopensource/applications/app-network_tools!5

---
